#include "stdafx.h"
#include "CppUnitTest.h"
#include <Dijkstra.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestesUnitarios
{		
	GrafoListaAdj<int, int> grafo;
	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_CLASS_INITIALIZE(InicializaGrafo) {
			grafo.carrega("entrada.txt");
		}
		TEST_METHOD(TesteCarrega)
		{
			Assert::AreEqual(2, grafo.listaAdj()[grafo.verticeToIndex(1)][0].vertice, L"Verificando primeiro v�rtice adjacente a 1", LINE_INFO());
			Assert::AreEqual(8, int(grafo.listaAdj().size()), L"Testando n�mero de v�rtices", LINE_INFO());
		}

		TEST_METHOD(TesteErroCarrega) {
			Assert::IsFalse(grafo.carrega("bla"), L"Testando carga de arquivo inexistente", LINE_INFO());
		}

		TEST_METHOD(TesteMenorCaminho1a7) {
			Assert::AreEqual(1, grafo.getAresta(8, 3));
			auto dijkstra = dijkstra::menorCaminho(1, grafo);
			auto vertice = dijkstra[grafo.verticeToIndex(7)];
			Assert::AreEqual(7, vertice.atual, L"V�rtice atual deve ser 7", LINE_INFO());
			Assert::AreEqual(2, vertice.anterior, L"V�rtice anterior", LINE_INFO());
			Assert::AreEqual(2, vertice.distancia, L"Dist�ncia de 1 a 8", LINE_INFO());
		}

		TEST_METHOD(TesteMenorCaminho1a8) {
			grafo.retiraAresta(3, 8);
			auto dijkstra = dijkstra::menorCaminho(1, grafo);
			auto vertice = dijkstra[grafo.verticeToIndex(8)];
			Assert::AreEqual(3, vertice.distancia, L"Dist�ncia entre 8 e 1 sem o v�rtice 3", LINE_INFO());
			Assert::AreEqual(7, vertice.anterior, L"V�rtice anterior a 8 no caminho at� 1", LINE_INFO());
		}

		TEST_METHOD(TesteArestaVazia) {
			auto aresta = grafo.getAresta(1, 7);
			Assert::AreEqual(0, aresta, L"Aresta n�o deve existir", LINE_INFO());
		}

	};
}