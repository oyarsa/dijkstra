var NAVTREE =
[
  [ "Dijkstra2", "index.html", [
    [ "Namespaces", null, [
      [ "Lista de namespaces", "namespaces.html", "namespaces" ],
      [ "Membros do namespace", "namespacemembers.html", [
        [ "Tudo", "namespacemembers.html", null ],
        [ "Funções", "namespacemembers_func.html", null ],
        [ "Definições de tipos", "namespacemembers_type.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Lista de componentes", "annotated.html", "annotated_dup" ],
      [ "Índice dos componentes", "classes.html", null ],
      [ "Componentes membro", "functions.html", [
        [ "Tudo", "functions.html", null ],
        [ "Funções", "functions_func.html", null ],
        [ "Variáveis", "functions_vars.html", null ],
        [ "Definições de tipos", "functions_type.html", null ]
      ] ]
    ] ],
    [ "Ficheiros", null, [
      [ "Lista de ficheiros", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_dijkstra_8cpp_source.html"
];

var SYNCONMSG = 'clique para desativar a sincronização do painel';
var SYNCOFFMSG = 'clique para ativar a sincronização do painel';