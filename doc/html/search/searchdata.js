var indexSectionsWithContent =
{
  0: "acdegilmnprtv",
  1: "gt",
  2: "d",
  3: "cegilmnprtv",
  4: "adpv",
  5: "gn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs"
};

var indexSectionLabels =
{
  0: "Tudo",
  1: "Classes",
  2: "Namespaces",
  3: "Funções",
  4: "Variáveis",
  5: "Definições de tipos"
};

