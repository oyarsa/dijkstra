﻿#pragma once

#include "GrafoListaAdj.h"
#include <limits>
#include <utility>
#include <vector>
#include <queue>

/// <summary> 
/// Namespace que contém as funções do algoritmo de Dijkstra para o menor caminho </summary>
///
/// <remarks>
/// Inclui a função que gera a lista de vértices com seus respectivos pesos e
/// vértices anteriores, e a função que rastreia o caminho da fonte a outro vértice
/// a partir dessa lista </remarks>
namespace dijkstra {
	/// <summary>
	/// Alias para o tipo do grafo. Ainda precisa ser instanciado com o template </summary>
	template <class Vertice, class Peso>
	using Grafo = GrafoListaAdj<Vertice, Peso>;

	/// <summary>
	/// Struct que vai guardar o vértice atual, o anterior e a distância até a fonte </summary>
	/// @tparam Vertice O tipo de vértices do grafo(ints, doubles, strings)
	/// @tparam Peso O tipo do peso das arestas do grafo (ints, doubles, booleans, structs)
	template <class Vertice, class Peso>
	struct TipoVizinho {
		/// <summary> Vértice no grafo <summary>
		Vertice atual;
		/// <summary> Vértice anterior no caminho até a fonte <summary>
		Vertice anterior;
		/// <summary> Distância até a fonte </summary>
		Peso distancia;

		/// <summary> 
		/// Construtor simples, apenas inicializa os membros com os argumentos </summary>
		TipoVizinho(Vertice atual, Vertice anterior, Peso distancia)
			: atual{atual},
			  anterior{anterior},
			  distancia{distancia} {}
	};

	/// <summary>
	/// Usa o algoritmo de Dijkstra para calcular o menor caminho da fonte
	/// aos outros vértices do grafo </summary>
	///
	/// @tparam Vertice O tipo de vértices do grafo (ints, doubles, strings)
	/// @tparam Peso O tipo do peso das arestas do grafo (ints, doubles, booleans, structs)
	/// <param name='fonte'> Referência const para o vértice que será a fonte </param>
	/// <param name='grafo'> Referência const para o grafo a ser percorrido </param>
	/// <returns> Lista de vizinhos (structs que contém o vértice atual, o anterior e
	/// a distância até a fonte </returns>
	template <class Vertice, class Peso>
	std::vector<TipoVizinho<Vertice, Peso>>
	menorCaminho(const Vertice& fonte, const Grafo<Vertice, Peso>& grafo) {

		// Constante para o infinito com o tipo do peso
		const auto inf = Peso(1e9);
		// Alias para o TipoVizinho, eliminando os argumentos de template
		using Vizinho = TipoVizinho<Vertice, Peso>;

		//  Tamanho do grafo e índice da fonte no vector de listas de adjacência
		auto tamanho = grafo.numVertices();
		auto indice = grafo.verticeToIndex(fonte);

		// Vector de vizinhos, será utilizado para guardar os valores de vértice
		// anterior e distância até a fonte
		std::vector<Vizinho> vizinhos{};

		// Insere cada vértice do grafo na lista, mas sem anterior e 
		// com distância infinita
		for (auto i = 0; i < tamanho; i++) {
			vizinhos.push_back(Vizinho{grafo.indexToVertice(i), Vertice{}, inf});
		}

		// Estabelece a distância da fonte até ela mesma como 0
		// (na verdade, o valor padrão para o tipo do peso). O anterior
		// continua sendo -1, pois não há vértice anterior à fonte
		vizinhos[indice].distancia = Peso{};

		// Alias para um par de vértice e peso. Será usado na fila de prioridade
		using P = std::pair<Vertice, Peso>;
		// Função lambda usada para instanciar a fila de prioridade. Compara apenas
		// as distâncias (segundo item do par) para definir a prioridade do item
		auto comp = [](const P& p1, const P& p2) {
			return p1.second > p2.second;
		};

		// Inicializa a fila de prioridade de pares usando um vector como container
		// (padrão) e a função de comparação comp para estabelecer a prioridade
		// Nota: o template da priority_queue, quando usado com uma lambda, requer 
		// que o tipo dela seja usado como parâmetro (e o decltype serve para 
		// deduzir esse tipo), enquanto a função propriamente dita entra como 
		// argumento para o construtor
		std::priority_queue<P, std::vector<P>, decltype(comp)> fila(comp);
		// Insere a fonte, com a sua respectiva distância, como primeiro item da fila
		fila.push(std::make_pair(fonte, Peso{}));

		// Executa enquanto ainda houverem vértices a serem explorados
		while (!fila.empty()) {
			// Desenfileira o vértice da fila de prioridade e pega seu índice na lista
			auto atual = fila.top();
			indice = grafo.verticeToIndex(atual.first);
			fila.pop();

			// Captura por referência a lista de adjacência do vértice (apenas leitura)
			const auto& adjacentes = grafo.listaAdj()[indice];

			// Percorre a lista de adjacência verificando se o caminho através 
			// do vértice atual é menor que o que o vizinho atualmente toma
			for (const auto& adj : adjacentes) {
				// Captura uma referência de leitura para um vizinho (o índice dele
				// no vector de vizinhos é o mesmo da lista de adjacência)
				auto& vizinho = vizinhos[grafo.verticeToIndex(adj.vertice)];
				// Calcula a distância pelo vértice atual
				auto distanciaPeloAtual = adj.peso + atual.second;

				// Compara se a distância é menor. Se for, modifica os valores do vizinho
				// na lista (a distância passa a ser esta e o anterior para a ser o
				// vértice atual) e insere o novo para na fila
				// Nota: se houver outra ocorrência desse vértice na fila, ela será 
				// descartada pois invariavelmente terá um caminho mais longo, 
				// então não entrará no condicional
				if (distanciaPeloAtual < vizinho.distancia) {
					vizinho.distancia = distanciaPeloAtual;
					vizinho.anterior = atual.first;
					fila.push(std::make_pair(adj.vertice, vizinho.distancia));
				}
			}

		}
		// Quando o algoritmo termina, a lista de vizinhos é retornada
		// Nota: à primeira vista ela é retornada por cópia, mas como é um valor 
		// pertencente ao escopo da função que não será mais utilizado, é na verdade
		// retornado por rvalue. Não há cópia, há apenas a mudança da posse do valor
		return vizinhos;
	}

	/// <summary>
	/// Rastreia o menor caminho da fonte ao destino usando uma lista de
	/// vizinhos gerada a partir da função menorCaminho, e retorna a lista de 
	/// vértices que o compõem </summary>
	///
	/// @tparam Vertice O tipo de vértices do grafo (ints, doubles, strings)
	/// @tparam Peso O tipo do peso das arestas do grafo (ints, doubles, booleans, structs)
	/// <param name='vizinhos'> Referência const para a lista de vizinhos
	/// gerada pelo algoritmo </param>
	/// <param name='fonte'> Referência const para um Vertice, a fonte. Deve ser 
	///				a mesma fonte da lista </param>
	/// <param name='destino'> Referência const para um Vertice, o destino </param>
	/// <param name='grafo'> Referência const para o grafo </param>
	/// <returns> Vector de Vertices que compõem o caminho da fonte ao destino
	/// (fonte e destino inclusos) </returns>
	template <class Vertice, class Peso>
	std::vector<Vertice>
	rastreiaCaminho(const std::vector<TipoVizinho<Vertice, Peso>>& vizinhos,
	                          const Vertice& fonte, const Vertice& destino,
	                          const Grafo<Vertice, Peso>& grafo) {

		// Inicializa o vector que irá guardar o caminho
		std::vector<Vertice> caminho{};
		// Começa do destino e vai rastreando de trás pra frente
		auto verticeAtual = destino;

		// Executa até chegar na fonte
		while (verticeAtual != fonte) {
			// Insere o vértice atual na lista
			caminho.push_back(verticeAtual);

			// Transforma o vértice anterior do atual no atual
			auto verticeAnterior = vizinhos[grafo.verticeToIndex(verticeAtual)].anterior;
			verticeAtual = std::move(verticeAnterior);
		}
		// Insere a fonte na lista
		caminho.push_back(fonte);

		// Retorna um vector invertido em relação ao vector caminho,
		// para então mostrar o caminho da fonte ao destino
		return std::vector<Vertice>(caminho.rbegin(), caminho.rend());
	}


} // namespace

