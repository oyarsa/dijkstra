Listas de adjacencia:

Astolfo_Dutra | CataguasesGuidovalRodeiroDona_Euzebia
Cataguases | Astolfo_DutraLeopoldinaMirai
Leopoldina | Cataguases
Tocantins | Rio_Pomba UbaPirauba
Rio_Pomba | TocantinsTabuleiro
Uba | TocantinsVisconde_do_Rio_BrancoGuidovalRodeiro
Mirai | CataguasesVisconde_do_Rio_BrancoMuriaeGuiricema
Visconde_do_Rio_Branco | Mirai UbaSao_GeraldoGuiricema
Sao_Geraldo | CoimbraVisconde_do_Rio_Branco
Coimbra | Sao_GeraldoVicosaMuriae
Vicosa | Coimbra
Muriae | CoimbraMirai
Guidoval |  UbaAstolfo_Dutra
Rodeiro |  UbaAstolfo_Dutra
Dona_Euzebia | Astolfo_Dutra
Tabuleiro | Rio_PombaPiau
Piau | TabuleiroCoronel_Pacheco
Coronel_Pacheco | PiauJuiz_de_ForaGoiana
Juiz_de_Fora | Coronel_Pacheco
Goiana | Coronel_PachecoRio_Novo
Rio_Novo | Goiana
Pirauba | GuaraniTocantins
Guarani | Pirauba
Guiricema | Visconde_do_Rio_BrancoMirai


Distancias a partir de Uba:

Vertice  Anterior   Distancia
Astolfo_Dutra   Rodeiro      44.2
CataguasesAstolfo_Dutra      70.5
LeopoldinaCataguases      94.1
Tocantins       Uba      12.7
Rio_Pomba Tocantins      37.6
 Uba                   0
MiraiVisconde_do_Rio_Branco        68
Visconde_do_Rio_Branco       Uba      21.8
Sao_GeraldoVisconde_do_Rio_Branco      34.9
CoimbraSao_Geraldo      48.2
Vicosa   Coimbra      69.2
Muriae     Mirai     102.8
Guidoval       Uba      25.3
Rodeiro       Uba      20.9
Dona_EuzebiaAstolfo_Dutra      55.6
Tabuleiro Rio_Pomba      52.5
Piau Tabuleiro      76.3
Coronel_Pacheco      Piau      91.3
Juiz_de_ForaCoronel_Pacheco     121.8
GoianaCoronel_Pacheco     107.6
Rio_Novo    Goiana     127.3
Pirauba Tocantins      33.6
Guarani   Pirauba      45.3
GuiricemaVisconde_do_Rio_Branco        37


Caminho de Uba a Juiz_de_Fora:
Uba Tocantins Rio_Pomba Tabuleiro Piau Coronel_Pacheco Juiz_de_Fora 

