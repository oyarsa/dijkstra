﻿#pragma once

#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <unordered_map>
#include <algorithm>

/// <summary> Nó do grafo, a ser inserido numa lista de adjacência. Contém
/// o vértice e o peso até o "dono" da lista </summary>
/// @tparam Vertice O tipo de vértices do grafo (ints, doubles, strings)
/// @tparam Peso O tipo do peso das arestas do grafo (ints, doubles, booleans, structs)
template <class Vertice, class Peso>
struct TipoNode {
	/// <summary> Vértice adjacente ao dono da lista </summary>
	Vertice vertice;
	/// <summary> Distância desse adjacente até o dono </summary>
	Peso peso;

	/// <summary> 
	/// Construtor simples, inicializa os membros com os argumentos </summary>
	TipoNode(Vertice vertice, Peso peso)
		: vertice{vertice},
		  peso{peso} {}

	/// <summary> Construtor vazio, inicializa com os valores padrão </summary>
	TipoNode()
		: vertice{},
		  peso{} {}
};

/// <summary>
/// Representa um grafo usando lista de adjacência, assim como
/// funções para a manipulação dos itens. Utiliza mapas para representar
/// qualquer tipo de dados como vértices. </summary>
/// @tparam Vertice O tipo de vértices do grafo (ints, doubles, strings)
/// @tparam Peso O tipo do peso das arestas do grafo (ints, doubles, booleans, structs)
template <class Vertice, class Peso>
class GrafoListaAdj {
public:
	/// <summary> 
	/// Alias para o TipoNode, evitando o uso dos parâmetros do template </summary>
	using Node = TipoNode<Vertice, Peso>;

	GrafoListaAdj();
	bool existeAresta(const Vertice& v1, const Vertice& v2) const;
	bool listaAdjacenciaVazia(const Vertice& v) const;
	const Vertice& primeiroAdjacente(const Vertice& v) const;
	const Peso& getAresta(const Vertice& v1, const Vertice& v2) const;
	void retiraAresta(const Vertice& v1, const Vertice& v2);
	bool imprime(const std::string nomeArquivo) const;
	void imprime() const;
	bool carrega(const std::string nomeArquivo);
	int numArestas() const;
	int numVertices() const;
	void insereAresta(const Vertice v1, const Vertice v2, const Peso p);
	int verticeToIndex(const Vertice& v) const;
	const Vertice& indexToVertice(int index) const;
	const std::vector<std::vector<Node>>& listaAdj() const;

private:
	/// <summary> Número de arestas do grafo </summary>
	int numArestas_;
	/// <summary> Número de vértices do grafo </summary>
	int numVertices_;
	/// <summary> Número de vértices inseridos no grafo </summary>
	int verticesInseridos;
	/// <summary> Vector de listas de adjacências dos vértices do grafo </summary>
	std::vector<std::vector<Node>> listaAdj_;
	/// <summary> Mapa entre vértices e índices deles no vector de listas </summary>
	std::unordered_map<Vertice, int> verticeToIndex_;
	/// <summary> 
	/// Mapa entre o índice de um vértice no vector de listas e o próprio </summary>
	std::unordered_map<int, Vertice> indexToVertice_;
	/// <summary> Representa se um grafo é direcionado ou não </summary>
	bool direcionado;
};

/// <summary> 
/// Construtor simples, inicializa os membros com os valores padrão. </summary>
template <class Vertice, class Peso>
GrafoListaAdj<Vertice, Peso>::GrafoListaAdj()
	:numArestas_{}, numVertices_{}, verticesInseridos{}, listaAdj_{},
	 verticeToIndex_{}, indexToVertice_{}, direcionado{} {}

/// <summary>
/// Retorna verdadeiro se existe alguma aresta entre v1 e v2, ou seja,
/// se v2 está na lista de adjacência de v1 </summary>
/// <param name='v1'> Referência const ao Vertice de origem v1 </param>
/// <param name='v2'> Referência const ao Vertice de destino v2 </param>
/// <returns> Verdadeiro se existe uma aresta que parte de v1 e vai a v2 </returns>
template <class Vertice, class Peso>
bool GrafoListaAdj<Vertice, Peso>::
existeAresta(const Vertice& v1, const Vertice& v2) const {
	// Captura referência à lista de adjacências do vértice v1
	const auto& listaAdjAtual = listaAdj_[verticeToIndex(v1)];
	// Pesquisa o vértice v2 entre os nodes da lista de adjacência
	// Se o valor retornado por find_if for o fim da lista, significa que
	// ele não está presente. Se for diferente, significa que o vértice está lá
	// (o valor retornado por find_if é um iterador apontando para a primeira ocorrência
	// do item pesquisado, e para o fim se ele não for encontrado)
	return std::find_if(begin(listaAdjAtual), end(listaAdjAtual), [&](Node node) {
		                    return node.vertice == v2;
	                    }) != end(listaAdjAtual);
}

/// <summary>
/// Recebe um vértice como argumento e retorna se sua lista de adjacência </summary>
/// <param name='v'> Referência const ao Vertice ao ser inspecionado </param>
/// <returns> Verdadeiro se a lista de adjacência do Vertice for vazia </returns>
template <class Vertice, class Peso>
bool GrafoListaAdj<Vertice, Peso>::
listaAdjacenciaVazia(const Vertice& v) const {
	// Retorna o booleano que o método empty do vector retornar,
	// verdadeiro se estiver vazio e falso se houver ao menos um item
	return listaAdj_[verticeToIndex(v)].empty();
}

/// <summary>
/// Retorna o primeiro vértice da lista de adjacência do vértice v </summary>
/// <param name='v'> Referência const ao Vertice a ser inspecionado </param>
/// <returns> Referência const ao primeiro Vertice adjacente a v </returns>
template <class Vertice, class Peso>
const Vertice& GrafoListaAdj<Vertice, Peso>::
primeiroAdjacente(const Vertice& v) const {
	// Se não houver vértices na lista de adjacência, retorna o valor padrão
	// para o tipo do vértice
	if (listaAdjacenciaVazia(v))
		return Vertice{};
	// Se houve, retorna o membro vértice do índice 0 da lista.
	return listaAdj_[verticeToIndex(v)][0].vertice;
}

/// <summary>
/// Retorna o valor da aresta entre v1 e v2, se existir. Se não existir,
/// retorna o valor padrão de Peso </summary>
/// <param name='v1'> Referência const ao Vertice de origem v1 </param>
/// <param name='v2'> Referência const ao Vertice de destino v2 </param>
/// <returns> Referência const ao Peso da aresta entre v1 e v2 </returns>
template <class Vertice, class Peso>
const Peso& GrafoListaAdj<Vertice, Peso>::
getAresta(const Vertice& v1, const Vertice& v2) const {
	// Captura referência const para a lista de adjacência de v1
	const auto& listaAdjAtual = listaAdj_[verticeToIndex(v1)];

	// Encontra o iterador que aponta para a primeira ocorrência de v2 na lista
	auto it = std::find_if(begin(listaAdjAtual), end(listaAdjAtual),
	                       [&v2] (Node n) {
		                      return n.vertice == v2;
	                       });
	// Se o iterador for diferente do fim da lista, retorna o 
	// peso do Node para o qual ele aponta
	if (it != end(listaAdjAtual))
		return it->peso;
	// Se não, retorna o valor padrão de Peso
	return Peso{};
}

/// <summary>
/// Retira a aresta de v1 a v2 do grafo, isto é, exclui v2 da lista
/// de adjacência de v1 </summary>
/// <param name="v1"> Referência const ao vértice de origem v1 </param>
/// <param name="v2"> Referência const ao vértice de destino v2 </param>
template <class Vertice, class Peso>
void GrafoListaAdj<Vertice, Peso>
::retiraAresta(const Vertice& v1, const Vertice& v2) {
	// Captura uma referência comum para a lista de adjacências de v1
	auto& adj = listaAdj_[verticeToIndex(v1)];

	// Remove as ocorrências de v2 da lista
	adj.erase(std::remove_if(begin(adj), end(adj), [&v2] (Node node) {
		                         return node.vertice == v2;
	                         }), end(adj));
}

/// <summary>
/// Imprime as listas de adjacência do grafo no arquivo nomeArquivo </summary>
/// <param name='nomeArquivo'> String com o nome do arquivo a ser impresso. Essa função 
/// irá sobrescrever qualquer conteúdo do arquivo </param>
/// <returns> Verdadeiro se o arquivo for escrito com sucesso. </returns>
template <class Vertice, class Peso>
bool GrafoListaAdj<Vertice, Peso>::
imprime(const std::string nomeArquivo) const {
	// Stream de saída para o arquivo nomeArquivo
	std::ofstream saida(nomeArquivo);

	// Se ocorreu algum problema ao abrir o arquivo, retorna falso
	if (!saida.is_open())
		return false;

	// Percore as listas de adjacência de todos os vértices do grafo, 
	// e imprime no arquivo os vértices adjacentes
	for (auto i = 0; i < numVertices_; i++) {
		saida << std::setw(3) << indexToVertice(i) << " | ";

		for (const auto& node : listaAdj_[i]) {
			saida << std::setw(4) << node.vertice;
		}
		saida << "\n";
	}

	return true;
}


/// <summary> Imprime as listas de adjacência do grafo na saída padrão </summary>
template <class Vertice, class Peso>
void GrafoListaAdj<Vertice, Peso>::imprime() const {
	// Percore as listas de adjacência de todos os vértices do grafo, 
	// e imprime na saída padrão os vértices adjacentes
	for (auto i = 0; i < numVertices_; i++) {
		std::cout << std::setw(3) << indexToVertice(i) << " | ";

		for (const auto& node : listaAdj_[i]) {
			std::cout << std::setw(4) << node.vertice;
		}
		std::cout << "\n";
	}
}

/// <summary>
/// Carrega o grafo a partir de um arquivo apropriadamente formatado </summary>
/// 
/// <remarks>
/// O arquivo deve estar formatado no seguinte padrão:
/// <code>
/// <número de vértices> <número de arestas> <direcionado ? 1 : 0>
/// <vértice 1> <peso da aresta entre os vértices> <vértice 2>
/// </code>
/// Onde a segunda linha acima aparece um número de vezes igual 
/// ao número de arestas </remarks>
/// <param name='nomeArquivo'> Nome do arquivo a ser lido </param>
/// <returns> Verdadeiro se não houver problemas ao ler o arquivo
/// Se o arquivo não for aberto corretamente, ele retornará falso. Se ocorrer
/// algum erro durante a leitura, uma exceção pode ser lançada </returns>
template <class Vertice, class Peso>
bool GrafoListaAdj<Vertice, Peso>::carrega(const std::string nomeArquivo) {
	// Stream de entrada que lê do arquivo nomeArquivo
	std::ifstream entrada(nomeArquivo);

	// Se ocorreu algum problema ao abrir o arquivo, retorna falso
	if (!entrada.is_open())
		return false;

	// Lê a primeira linha do arquivo, com o número de vértices, aresta, e
	// o booleano que informa se o grafo é direcionado ou não
	entrada >> numVertices_ >> numArestas_ >> direcionado;
	// Inicializa o vector de listas de adjacências com numVertices_ vectors de nodes
	listaAdj_ = std::vector<std::vector<Node>>(numVertices_);

	// Variáveis para capturar as informações lidas do arquivo
	Vertice v1, v2;
	Peso peso;

	// Lê informações do arquivo até seu fim
	while (entrada >> v1 >> peso >> v2) {
		insereAresta(v1, v2, peso);
		// Se o grafo não for direcionado, insere a direção inversa
		if (!direcionado)
			insereAresta(v2, v1, peso);
	}

	return true;
}

/// <summary> Retorna o número de arestas do grafo </summary>
/// <returns> Um inteiro, o número de arestas do grafo </returns>
template <class Vertice, class Peso>
int GrafoListaAdj<Vertice, Peso>::numArestas() const {
	return numArestas_;
}

/// <summary> Retorna o número de vértices do grafo </summary>
/// <returns> Um inteiro, o número de vértices do grafo </returns>
template <class Vertice, class Peso>
int GrafoListaAdj<Vertice, Peso>::numVertices() const {
	return numVertices_;
}

/// <summary> Insere uma aresta entre dois vértices do grafo </summary>
/// <param name='v1'> Vertice v1 a ser adicionado no grafo </param>
/// <param name='v2'> Vertice v2 a ser adicionado no grafo </param>
/// <param name='p'> Peso da aresta entre v1 e v2 </param>
template <class Vertice, class Peso>
void GrafoListaAdj<Vertice, Peso>::
insereAresta(const Vertice v1, const Vertice v2, const Peso p) {
	// Testa se o vértice ainda não foi inserido no grafo (e, por consequência,
	// nos mapas). Se for o caso, insere o novo índices nos mapas e incrementa o número
	// de vértices inseridos
	if (verticeToIndex_.find(v1) == end(verticeToIndex_)) {
		verticeToIndex_[v1] = verticesInseridos;
		indexToVertice_[verticesInseridos] = v1;
		verticesInseridos++;
	}

	// Insere um Node de Vertice v2 e Peso p na lista de adjacências de v1
	listaAdj_[verticeToIndex(v1)].push_back(Node(v2, p));
}

/// <summary> 
/// Retorna o índice do Vertice v no vector de listas de adjacência </summary>
/// <param name='v'> Referência const ao vértice a ser inspecionado </param>
/// <returns> Um inteiro, o índice de v no vector de listas de adjacência </returns>
template <class Vertice, class Peso>
int GrafoListaAdj<Vertice, Peso>::verticeToIndex(const Vertice& v) const {
	// Retorna o valor do índice de v no mapa. Se o item não existir, lança
	// uma exceção out_of_range
	return verticeToIndex_.at(v);
}

/// <summary>
/// Retorna uma referência const ao vértice na posição i do vector
/// de listas de adjacência </summary>
/// <param name='index'> Índice no vector de listas de adjacência </param>
/// <returns> Referência const ao vértice </returns>
template <class Vertice, class Peso>
const Vertice& GrafoListaAdj<Vertice, Peso>::indexToVertice(int index) const {
	// Retorna uma referência const ao vértice na posição index do mapa
	// Se o item não existir, lança uma exceção out_of_range
	return indexToVertice_.at(index);
}

/// <summary> 
/// Retorna uma referência const ao vector de listas de adjacência </summary>
/// <returns> Referência const a um vector de vectors de Nodes </returns>
template <class Vertice, class Peso>
const std::vector<std::vector<typename GrafoListaAdj<Vertice, Peso>::Node>>&
GrafoListaAdj<Vertice, Peso>::listaAdj() const {
	return listaAdj_;
}
