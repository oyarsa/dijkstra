#include "GrafoListaAdj.h"
#include "Dijkstra.h"
#include <iostream>
#include <iterator>
#include <string>
//#include <cstdio>

// Alias para o tipo do grafo
template <class Vertice, class Peso>
using Grafo = GrafoListaAdj<Vertice, Peso>;

int main() {
	//freopen("saidaCidades.txt", "w", stdout);
	// Alias para os tipos de v�rtice e peso
	//using Vertice = int;
	using Vertice = std::string;
	using Peso = double;

	// Inicializa o grafo e o carrega, depois testa se foi bem sucedido
	// OBS: � o mesmo grafo do exerc�cio do laborat�rio, mas ser� retirada a aresta
	// de 3 a 8 para fins de teste
	Grafo<Vertice, Peso> grafo;
	//if (!grafo.carrega("entrada.txt")) {
	if (!grafo.carrega("cidades.txt")) {
		std::cout << "Erro ao abrir o arquivo.\n";
		return 1;
	}

	// Retira a aresta de 3 para 8 do grafo
	/*grafo.retiraAresta(3, 8);
	grafo.retiraAresta(1, 2);*/

	// Imprime as listas de adjac�ncia do grafo na sa�da padr�o
	std::cout << "Listas de adjacencia:\n\n";
	grafo.imprime();

	// Calcula o menor caminho de 1 para o resto do grafo 
	// e guarda a lista de vizinhos
	//auto fonte = 1;
	std::string fonte{"Uba"};
	const auto menorCaminho = dijkstra::menorCaminho(fonte, grafo);

	// Percorre a lista de vizinhos e imprime os v�rtices anteriores no caminho
	// at� a fonte, assim como as dist�ncias at� a fonte, na sa�da padr�o
	std::cout << "\n\nDistancias a partir de " << fonte << ":\n";
	std::cout << "\n" << "Vertice" << std::setw(10)
			<< "Anterior" << std::setw(10) << "   Distancia\n";
	for (const auto& vizinho : menorCaminho) {
		std::cout << std::setw(4) << vizinho.atual << std::setw(10)
				<< vizinho.anterior << std::setw(10) << vizinho.distancia << "\n";
	}
	std::cout << "\n";

	// Rastreia o caminho do destino at� a fonte, e guarda a lista de v�rtices
	//auto destino = 8;
	std::string destino{"Juiz_de_Fora"};
	auto caminho = dijkstra::rastreiaCaminho(menorCaminho, fonte, destino, grafo);

	// Imprime a lista de v�rtices na sa�da padr�o
	std::cout << "\nCaminho de " << fonte << " a " << destino << ":\n";
	copy(begin(caminho), end(caminho), std::ostream_iterator<Vertice>(std::cout, " "));
	std::cout << "\n\n";

	return 0;
}
