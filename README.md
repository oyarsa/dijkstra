# Implementação do algoritmo de Dijkstra #
para encontrar o menor
caminho de uma fonte a todos os vértices do grafo.
    

Utiliza templates para aceitar qualquer tipo como identificador para
os vértices, e qualquer forma de peso entre arestas, desde que seja
comparável.